const path = require('path');

module.exports = {
    useDocker: false,
    screenDir: path.join(__dirname, '../images'),
    browsers: {
        chrome: {
            browserName: 'chrome',
            viewport: { width: 1400, height: 720 },
            gridUrl: 'http://selenoid__chrome:4444/',
        },
    },
};
